<?php

get_header();

?>

<div class="blog-main-content">
    <div class="container main-content blog-container">
        <div class="main-content-blog-heading main-heading">
            <h1><?php single_cat_title(); ?></h1>
            <p></p>
        </div>
        <?php
        if( have_posts() ) :
        $counter = 0;
            ?>
            <div class="blog-images">
                <?php
                while( have_posts() ) :
                    $counter++;
                    the_post();
                    $backgroundImage = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'full' );
                    if( $counter == 1 || $counter == 6 ) {
                        $class = 'big-img';
                    } else {
                        $class = 'small-img';
                    }
                    if( $counter == 1 || $counter == 6 || $counter == 2 || $counter == 4 ) {
                        ?>
                        <div class="img-wrapper-blog">
                        <?php
                    }
                    ?>
                    <div class="<?php echo $class; ?>" style="background-image: url('<?php echo $backgroundImage[0]; ?>')">
                        <div class="img-wrapper-blog-content">
                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                            <a class="blog-post-link" href="<?php the_permalink(); ?>"><span class="sr-only"><?php _e('Read more...'. 'ibis'); ?></span></a>
                        </div>
                    </div>
                    <?php
                    if( $counter == 1 || $counter == 6 || $counter == 3 || $counter == 5 ) {
                        ?>
                        </div>
                        <?php
                    }
                endwhile;
                ?>
            </div>
            <?php
            global $wp_query;
            $big = 999999999;
            ?>
            <div class="blog-navigation">
                <?php
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var( 'paged' ) ),
                    'total' => $wp_query->max_num_pages
                ) );
                ?>
            </div>
            <?php
            wp_reset_postdata();
        else :
            _e( 'Sorry, no content found.', 'ibis' );
        endif;
        ?>
    </div>
</div>

<?php

get_footer();
