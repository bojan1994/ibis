<?php

/* Register scripts */
if( ! function_exists( 'register_scripts' ) ) {
    function register_scripts() {
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ) );
        //wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/assets/js/jquery.matchHeight-min.js', array( 'jquery' ) );
        wp_enqueue_script( 'form', get_template_directory_uri() . '/assets/js/form.js', array( 'jquery' ), false, true );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array( 'bootstrap' ), false, true );
        wp_enqueue_style( 'style', get_stylesheet_uri() );
    }
add_action( 'wp_enqueue_scripts', 'register_scripts' );
}

/* Register Navigation Menus */
register_nav_menus( array(
    'primary' => __( 'Primary Navigation' ),
    'secondary' => __( 'Secondary Navigation' ),
    'footer' => __( 'Footer Primary Navigation' ),
    'footer2' => __( 'Footer Secondary Navigation' ),
) );

/* Theme Setup */
if( ! function_exists( 'theme_setup' ) ) {
    function theme_setup() {
        $array = array(
            'height'      => 80,
            'width'       => 150,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $array );
        add_image_size( 'slider-image', 1920, 1080, true );
        add_image_size( 'blog-image-big', 580, 520, true );
        add_image_size( 'blog-image', 400, 250, true );
        add_image_size( 'blog-image-full', 1170, 615, true );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );
    }
    add_action( 'after_setup_theme', 'theme_setup' );
}

/* Custom footer */
if( ! function_exists( 'theme_customizer' ) ) {
    function theme_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_footer_text',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text',array(
            'title' => __( 'Custom footer text','Ibis' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'custom_footer',array(
            'label' => __( 'Footer Text','Ibis' ),
            'section' => 'footer_text',
            'settings' => 'custom_footer_text',
        ) ) );
    }
    add_action( 'customize_register','theme_customizer' );
}

/* Facebook url */
if( ! function_exists( 'add_facebook_url' ) ) {
    function add_facebook_url( $wp_customize ) {
        $wp_customize->add_setting( 'custom_facebook_url',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'facebook_url',array(
            'title' => __( 'Facebook url','Ibis' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'facebook_url',array(
            'label' => __( 'Facebook url','Ibis' ),
            'section' => 'facebook_url',
            'settings' => 'custom_facebook_url',
        ) ) );
    }
    add_action( 'customize_register','add_facebook_url' );
}

/* Twitter url */
if( ! function_exists( 'add_twitter_url' ) ) {
    function add_twitter_url( $wp_customize ) {
        $wp_customize->add_setting( 'custom_twitter_url',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'twitter_url',array(
            'title' => __( 'Twitter url','Ibis' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'twitter_url',array(
            'label' => __( 'Twitter url','Ibis' ),
            'section' => 'twitter_url',
            'settings' => 'custom_twitter_url',
        ) ) );
    }
    add_action( 'customize_register','add_twitter_url' );
}

/* Instagram url */
if( ! function_exists( 'add_instagram_url' ) ) {
    function add_instagram_url( $wp_customize ) {
        $wp_customize->add_setting( 'custom_instagram_url',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'instagram_url',array(
            'title' => __( 'Instagram url','Ibis' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'instagram_url',array(
            'label' => __( 'Instagram url','Ibis' ),
            'section' => 'instagram_url',
            'settings' => 'custom_instagram_url',
        ) ) );
    }
    add_action( 'customize_register','add_instagram_url' );
}

/* Linkedin url */
if( ! function_exists( 'add_linkedin_url' ) ) {
    function add_linkedin_url( $wp_customize ) {
        $wp_customize->add_setting( 'custom_linkedin_url',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'linkedin_url',array(
            'title' => __( 'Linkedin url','Ibis' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'linkedin_url',array(
            'label' => __( 'Linkedin url','Ibis' ),
            'section' => 'linkedin_url',
            'settings' => 'custom_linkedin_url',
        ) ) );
    }
    add_action( 'customize_register','add_linkedin_url' );
}

// Register widgets
function addWidget(){
    register_sidebar( array (
        'name' => 'Footer image',
        'id' => 'footer1'
    ));
}
add_action( 'widgets_init', 'addWidget' );

/* Register slider */
if( ! function_exists( 'slider_post_type' ) ) {
    function slider_post_type() {
        $labels = array(
            'name' => __( 'Homepage Slider', 'Ibis' ),
            'singular_name' => __( 'Homepage Slider', 'Ibis' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider',
            ),
        );
        register_post_type( 'slider', $args );
    }
    add_action( 'init','slider_post_type' );
}

/* Register About Us slider */
if( ! function_exists( 'slider_post_type1' ) ) {
    function slider_post_type1() {
        $labels = array(
            'name' => __( 'About Us Slider', 'Ibis' ),
            'singular_name' => __( 'About Us Slider', 'Ibis' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
            'revisions'
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider1',
            ),
        );
        register_post_type( 'slider1', $args );
    }
    add_action( 'init','slider_post_type1' );
}

/* Register Customer Analytics slider */
if( ! function_exists( 'slider_post_type2' ) ) {
    function slider_post_type2() {
        $labels = array(
            'name' => __( 'Customer Analytics Slider', 'Ibis' ),
            'singular_name' => __( 'Customer Analytics Slider', 'Ibis' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
            'revisions'
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider2',
            ),
        );
        register_post_type( 'slider2', $args );
    }
    add_action( 'init','slider_post_type2' );
}

/* Register Network Analytics slider */
if( ! function_exists( 'network_analytics_slider' ) ) {
    function network_analytics_slider() {
        $labels = array(
            'name' => __( 'Network Analytics Slider', 'Ibis' ),
            'singular_name' => __( 'Network Analytics Slider', 'Ibis' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
            'revisions'
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'network_slider',
            ),
        );
        register_post_type( 'network_slider', $args );
    }
    add_action( 'init','network_analytics_slider' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box' ) ) {
    function contact_meta_box() {
        add_meta_box(
            'contaact_box',
            'Contact',
            'contact_meta_box_callback',
            'slider'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box' );
}
if( ! function_exists( 'contact_meta_box_callback' ) ) {
    function contact_meta_box_callback( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data', 'contact_meta_box_nonce' );
        $value = get_post_meta( $post->ID, '_contact_value_key', true );
        $url = get_post_meta( $post->ID, '_contact_url_value_key', true );
        echo '<label for="contact_label">Label: </label>';
        echo '<input type="text" id="contact_label" name="contact_label" value="' . esc_attr( $value ) . '" size="25" />';
        echo '<br>';
        echo '<label for="contact_url">Url: </label>';
        echo '<input type="text" id="contact_url" name="contact_url" value="' . esc_attr( $url ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data' ) ) {
    function contact_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce'], 'contact_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label'] ) ) {
            return;
        }
        $data1 = sanitize_text_field( $_POST['contact_label'] );
        update_post_meta( $post_id, '_contact_value_key', $data1 );
        $url1 = $_POST['contact_url'];
        update_post_meta( $post_id, '_contact_url_value_key', $url1 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data' );
}

if ( ! function_exists( 'vc_before_init_actions' ) ) {
    function vc_before_init_actions() {
        require_once( get_template_directory() . '/vc-templates/homepage-slider.php' );
        require_once( get_template_directory() . '/vc-templates/homepage.php' );
        require_once( get_template_directory() . '/vc-templates/about-us-slider.php' );
        require_once( get_template_directory() . '/vc-templates/about-us.php' );
        require_once( get_template_directory() . '/vc-templates/customer-analytics-slider.php' );
        require_once( get_template_directory() . '/vc-templates/customer-analytics.php' );
        require_once( get_template_directory() . '/vc-templates/network-analytics-slider.php' );
        require_once( get_template_directory() . '/vc-templates/network-analytics.php' );
        require_once( get_template_directory() . '/vc-templates/request.php' );
        require_once( get_template_directory() . '/vc-templates/scroll-spy.php' );
        require_once( get_template_directory() . '/vc-templates/test.php' );
    }
    add_action( 'vc_before_init', 'vc_before_init_actions' );
}

function posts_in_category( $query ){
    if ( $query->is_category ) {
        $query->set( 'posts_per_archive_page', 6 );
    }
}

add_filter('pre_get_posts', 'posts_in_category');
