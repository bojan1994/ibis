<?php

if(isset($_POST['btn_submit1'])) {
    if(!empty($_POST['fullname']) && !empty($_POST['email']) && !empty($_POST['message'])) {
        $to = 'bojan.mihajlovic@m1.rs';
        $subject = 'Contact form message';
        $message = strip_tags($_POST['message']);
        $fullname = strip_tags($_POST['fullname']);
        $email = strip_tags($_POST['email']);
        $headers = array();
        $headers[] = "From: $fullname <$email>";
        $mail = wp_mail($to,$subject,$message,$headers);
        if($mail) {
            echo '<script>jQuery(window).load(function(){jQuery(\'#form-message-ibis\').text(\'Message has been sent\');});</script>';
        }
    } else {
        echo '<script>jQuery(window).load(function(){jQuery(\'#form-message-ibis\').text(\'Message could not be sent\');});</script>';
    }
}
