<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__ . '/../vendor/autoload.php';

if (isset($_POST['btn_submit3']) && $_POST['btn_submit3'] != "") {
    if (!empty($_POST['fullname']) && !empty($_POST['company']) && !empty($_POST['position']) && !empty($_POST['email'])) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $fullname = strip_tags($_POST['fullname']);
            $company = strip_tags($_POST['company']);
            $position = strip_tags($_POST['position']);
            $email = strip_tags($_POST['email']);
            $message = strip_tags($_POST['message']);
            $mail = new PHPMailer(true);
            $mail->setFrom($email, $fullname);
            $mail->addAddress('bojan.mihajlovic@m1.rs','Bojan Mihajlovic');
            $mail->addReplyTo($email, $fullname);
            $mail->isHTML(true);
            $mail->Subject = 'Case Study Request';
            $mail->Body    = 'Company: ' . $company;
            $mail->Body    .= '<br>';
            $mail->Body    .= 'Position: ' . $position;
            $mail->Body    .= '<br>';
            $mail->Body    .= 'Message: ' . $message;
            $mail->AltBody = $message;

            $success = $mail->send();
            if($success) {
                echo 'Message has been sent';
                echo '<br>';
                echo '<a href="http://ivankosutic.x3.rs/ibis/wp-content/uploads/2018/07/ipipdf2.pdf" target="_blank">Thank you. Here is download link for the case study.</a>';
                }
        } else {
            http_response_code(403);
            echo 'Invalid email format';
        }
    }
} else {
    http_response_code(403);
}
