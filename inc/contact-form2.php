<?php

if(isset($_POST['btn_submit2'])) {
    if(!empty($_POST['fullname']) && !empty($_POST['company']) && !empty($_POST['position']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['message'])) {
        $to = 'bojan.mihajlovic@m1.rs';
        $subject = 'Demo Request';
        $fullname = strip_tags($_POST['fullname']);
        $company = strip_tags($_POST['company']);
        $position = strip_tags($_POST['position']);
        $email = strip_tags($_POST['email']);
        $phone = strip_tags($_POST['phone']);
        $message = strip_tags($_POST['message']);
        $headers = array();
        $headers[] = "From: $fullname <$email>";
        $mail = wp_mail($to,$subject,$message,$headers);
        if($mail) {
            echo '<script>jQuery(window).load(function(){jQuery(\'#form-message-ibis\').text(\'Message has been sent\');});</script>';
        }
    } else {
        echo '<script>jQuery(window).load(function(){jQuery(\'#form-message-ibis\').text(\'Message could not be sent\');});</script>';
    }
}
