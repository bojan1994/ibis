<?php

get_header();

if( have_posts() ) :
    while( have_posts() ) :
        the_post(); ?>
        <?php the_content();
    endwhile;
else :
    _e('Sorry, no content found.', 'ibis');
endif;

get_footer();
