<?php

$args = array(
    'post_type' => 'blog_slider',
);
$query = new WP_Query( $args );
if( $query->have_posts() ) : ?>
    <div id="main-carousel" class="carousel section slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <?php
            $counter = 0;
                while( $query->have_posts() ) :
                    $counter++;
                    $query->the_post(); ?>
                        <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                            <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => 'Blog slider' ) ); ?>
                            <div class="carousel-caption">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>
                        </div>
            <?php endwhile;
            wp_reset_query();
            if ( $counter != 1 ) {
                ?>
                <ol class="carousel-indicators">
                <?php
                for( $i=0; $i<$counter; $i++ ) {
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                    <?php
                } ?>
                </ol>
                <?php
            } ?>
        </div>
    </div>
<?php else :
    get_template_part( '../content', 'none' );
endif;
