var $ = jQuery.noConflict();
$('#qwertyuiop').on('submit', function(e){

    var json = {};
    var ok = true;

    ["fullName", "company", "position", "email", "message"].forEach(function(val) {
            var value = $("[name=" + val + "]").val();
            json[val] = value;

            if(!value && val != "message") ok = false;
    });


    e.preventDefault();
    var fullName = $('input[name="fullname"]').val(),
        company  = $('input[name="company"]').val(),
        position = $('input[name="position"]').val(),
        email    = $('input[name="email"]').val(),
        message  = $('textarea[name="message"]').val(),
        btnSubmit = "";

    if (fullName != '' && company != '' && position != '' && email != '') {
        btnSubmit = "true";
    }
    var json = {
        fullname: fullName,
        company: company,
        position: position,
        email: email,
        message: message,
        btn_submit3: btnSubmit
    }

    $.post('wp-content/themes/ibis/inc/contact-form3.php', json).done(function(data){
        $('#qwertyuiop').empty();
        $('#qwertyuiop').replaceWith('<div id="qwertyuiop"><p>' + data + '</p></div>');
    }).fail(function(data){
        if(json.email == '' || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(json.email)) {
            $('#error-form-on-submit').text('Message could not be sent!');
        } else if(json.fullname == '') {
            $('#error-form-on-submit').text('Please enter your fullname');
        } else if(json.company == '') {
            $('#error-form-on-submit').text('Please enter your company');
        } else if(json.position == '') {
            $('#error-form-on-submit').text('Please enter your position');
        } else {
            $('#error-form-on-submit').text('Invalid e-mail');
        }

    });
});
