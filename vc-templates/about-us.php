<?php

class vcIbisAboutUs extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_about_us_mapping' ) );
        add_shortcode( 'vc_ibis_about_us', array( $this, 'vc_ibis_about_us_html' ) );
    }
    public function vc_ibis_about_us_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'About Us', 'ibis' ),
                'base' => 'vc_ibis_about_us',
                'description' => __( 'About Us', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-about-us-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-about-us-title',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-about-us-content',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-about-us-first-image',
                        'heading' => __( 'Image 1', 'ibis' ),
                        'param_name' => 'image1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-about-us-first-title',
                        'heading' => __( 'Title 1', 'ibis' ),
                        'param_name' => 'title1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-about-us-first-content',
                        'heading' => __( 'Content 1', 'bilbrod' ),
                        'param_name' => 'content1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-about-us-second-image',
                        'heading' => __( 'Image 2', 'ibis' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-about-us-second-title',
                        'heading' => __( 'Title 2', 'ibis' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-about-us-second-content',
                        'heading' => __( 'Content 2', 'bilbrod' ),
                        'param_name' => 'content2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-about-us-third-image',
                        'heading' => __( 'Image 3', 'ibis' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-about-us-third-title',
                        'heading' => __( 'Title 3', 'ibis' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-about-us-third-content',
                        'heading' => __( 'Content 3', 'bilbrod' ),
                        'param_name' => 'content3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-about-us-label',
                        'heading' => __( 'Label', 'bilbrod' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'a',
                        'class' => 'ibis-about-us-url',
                        'heading' => __( 'URL', 'bilbrod' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-about-us-bottom-image',
                        'heading' => __( 'Image 4', 'ibis' ),
                        'param_name' => 'image4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'About Us',
                    ),
                )
            )
        );
    }
    public function vc_ibis_about_us_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'title' => '',
                    'paragraph' => '',
                    'image1' => '',
                    'title1' => '',
                    'content1' => '',
                    'image2' => '',
                    'title2' => '',
                    'content2' => '',
                    'image3' => '',
                    'title3' => '',
                    'content3' => '',
                    'label' => '',
                    'url' => '',
                    'image4' => '',
                ),
                $atts
            )
        );
        $html = '';
        $html .= '<div class="main-content section main-content-about" id="main-content-about">';
        $html .= '<div class="container">';
        $html .= '<div class="main-content-about-heading main-heading">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image, 'full', false )[0] . '" alt="About Ibis Instruments">';
        $html .= '<h2>' . $title . '</h2>';
        $html .= '<p></p>';
        $html .= '</div>';
        $html .= '<div class="main-content-about-boxes main-content-boxes">';
        $html .= '<p>' . $paragraph . '</p>';
        $html .= '<div class="main-content-about-boxes-images main-content-boxes-images" id="main-content-about-boxes-images">';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-4 main-content-about-boxes-images main-content-about-boxes-images-one">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image1, 'full', false )[0] . '" alt="22 YEARS IN BUSSINESS">';
        $html .= '<h4>' . $title1 . '</h4>';
        $html .= '<p>' . $content1 . '</p>';
        $html .= '</div>';
        $html .= '<div class="col-md-4 main-content-about-boxes-images main-content-about-boxes-images-two">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image2, 'full', false )[0] . '" alt="MORE THEN 70 EMPLOYEES">';
        $html .= '<h4>' . $title2 . '</h4>';
        $html .= '<p>' . $content2 . '</p>';
        $html .= '</div>';
        $html .= '<div class="col-md-4 main-content-about-boxes-images main-content-about-boxes-images-three">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image3, 'full', false )[0] . '" alt="OVER 300 CLIENTS">';
        $html .= '<h4>' . $title3 . '</h4>';
        $html .= '<p>' . $content3 . '</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<a href="' . $url . '" class="main-content-about-box-button button-animation">' . $label . '</a>';
        $html .= '</div>';
        $html .= '<div class="main-content-about-bottom-image main-content-bottom-image">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image4, 'full', false )[0] . '" alt="Bottom image">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}

new vcIbisAboutUs();
