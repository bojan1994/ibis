<?php

class vcIbisHomepageSlider extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_homepage_slider_mapping' ) );
        add_shortcode( 'vc_ibis_homepage_slider', array( $this, 'vc_ibis_homepage_slider_html' ) );
    }
    public function vc_ibis_homepage_slider_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Homepage Slider', 'ibis' ),
                'base' => 'vc_ibis_homepage_slider',
                'description' => __( 'Homepage Slider', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Homepage Slider',
                    ),
                )
            )
        );
    }
    public function vc_ibis_homepage_slider_html( $atts ) {
        $html = '';
        global $post;
        $args = array(
        	'post_type' => 'slider',
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) : ?>
            <div id="main-carousel" class="carousel section slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $counter = 0;
                        while( $query->have_posts() ) :
                            $counter++;
                            $query->the_post(); ?>
                                <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                                    <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => 'Homepage slider' ) ); ?>
                                    <div class="carousel-caption">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_content(); ?>
                                        <?php
                                        $contact_url = get_post_meta( $post->ID,'_contact_url_value_key',true );
                                        $contact_label = get_post_meta( $post->ID,'_contact_value_key',true );
                                        if( $contact_label && $contact_url ) {
                                            ?>
                                            <a href="<?php echo $contact_url; ?>" class="carousel-button carousel-button-animation"><?php echo $contact_label; ?></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                    <?php endwhile;
                    wp_reset_query();
                    if ( $counter != 1 ) {
                        ?>
                        <ol class="carousel-indicators">
                        <?php
                        for( $i=0; $i<$counter; $i++ ) {
                            ?>
                            <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                            <?php
                        } ?>
                        </ol>
                        <?php
                    } ?>
                </div>
            </div>
        <?php else :
            get_template_part( '../content', 'none' );
        endif;
        return $html;
    }
}

new vcIbisHomepageSlider();
