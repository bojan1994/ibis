<?php

class vcIbisNetworkAnalytics extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_network_analytics_mapping' ) );
        add_shortcode( 'vc_ibis_network_analytics', array( $this, 'vc_ibis_network_analytics_html' ) );
    }
    public function vc_ibis_network_analytics_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Network Analytics', 'ibis' ),
                'base' => 'vc_ibis_network_analytics',
                'description' => __( 'Network Analytics', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-network-analytics-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-network-analytics-title',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-network-analytics-subtitle',
                        'heading' => __( 'Subtitle', 'ibis' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-network-analytics-content',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-network-analytics-first-section-image',
                        'heading' => __( 'Feature Image', 'ibis' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-network-analytics-subtitle2',
                        'heading' => __( 'Subtitle 2', 'ibis' ),
                        'param_name' => 'subtitle2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-network-analytics-content2',
                        'heading' => __( 'Content 2', 'ibis' ),
                        'param_name' => 'paragraph2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-network-analytics-second-section-image',
                        'heading' => __( 'Feature Image 2', 'ibis' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-customer=analytics-bottom-image',
                        'heading' => __( 'Image 3', 'ibis' ),
                        'param_name' => 'image4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Network Analytics',
                    ),
                )
            )
        );
    }
    public function vc_ibis_network_analytics_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'title' => '',
                    'subtitle' => '',
                    'paragraph' => '',
                    'image2' => '',
                    'subtitle2' => '',
                    'paragraph2' => '',
                    'image3' => '',
                    'image4' => '',
                ),
                $atts
            )
        );
        $html = '';
        $html .= '<div class="main-content section main-content-network">';
        $html .= '<div class="container">';
        $html .= '<div class="main-content-network-heading main-heading">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image, 'full', false )[0] . '" alt="Network Analytics">';
        $html .= '<h2>' . $title . '</h2>';
        $html .= '<p></p>';
        $html .= '</div>';
        $html .= '<div class="main-content-network-boxes section main-content-boxes">';
        $html .= '<div class="main-content-network-top-box" id="main-content-network-top-box">';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-8 main-content-network-top-box-left">';
        $html .= '<h4>' . $subtitle . '</h4>';
        $html .= '<p>' . $paragraph . '</p>';
        $html .= '</div>';
        $html .= '<div class="col-md-4 section main-content-network-top-box-right">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image2, 'full', false )[0] . '" alt="Network Operations Center">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="main-content-network-bottom-box" id="main-content-network-bottom-box">';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-8 main-content-network-bottom-box-left">';
        $html .= '<h4>' . $subtitle2 . '</h4>';
        $html .= '<p>' . $paragraph2 . '</p>';
        $html .= '</div>';
        $html .= '<div class="col-md-4 main-content-network-bottom-box-right">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image3, 'full', false )[0] . '" alt="Field Operations">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="main-content-network-bottom-image main-content-bottom-image">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image4, 'full', false )[0] . '" alt="Bottom image">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}

new vcIbisNetworkAnalytics();
