<?php

class vcIbisAboutUsSlider extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_about_us_slider_mapping' ) );
        add_shortcode( 'vc_ibis_about_us_slider', array( $this, 'vc_ibis_about_us_slider_html' ) );
    }
    public function vc_ibis_about_us_slider_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'About Us Slider', 'ibis' ),
                'base' => 'vc_ibis_about_us_slider',
                'description' => __( 'About Us Slider', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'About Us slider',
                    ),
                )
            )
        );
    }
    public function vc_ibis_about_us_slider_html( $atts ) {
        $html = '';
        $args = array(
        	'post_type' => 'slider1',
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) : ?>
            <div id="main-carousel" class="carousel section slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $counter = 0;
                        while( $query->have_posts() ) :
                            $counter++;
                            $query->the_post(); ?>
                                <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                                    <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => 'About Us' ) ); ?>
                                    <div class="carousel-caption">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                    <?php endwhile;
                    wp_reset_query();
                    if ( $counter != 1 ) {
                        ?>
                        <ol class="carousel-indicators">
                        <?php
                        for( $i=0; $i<$counter; $i++ ) {
                            ?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                            <?php
                        } ?>
                        </ol>
                        <?php
                    } ?>
                </div>
            </div>
        <?php else :
            get_template_part( '../content', 'none' );
        endif;
        return $html;
    }
}

new vcIbisAboutUsSlider();
