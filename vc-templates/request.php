<?php

class vcIbisRequest extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_request_mapping' ) );
        add_shortcode( 'vc_ibis_request', array( $this, 'vc_ibis_request_html' ) );
    }
    public function vc_ibis_request_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Request a demo', 'ibis' ),
                'base' => 'vc_ibis_request',
                'description' => __( 'Request a demo', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-request-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Request a demo',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-requestt-title',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Request a demo',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-request-subtitle',
                        'heading' => __( 'Subtitle', 'ibis' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Request a demo',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-request-bottom-image',
                        'heading' => __( 'Bottom image', 'ibis' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Request a demo',
                    ),
                )
            )
        );
    }
    public function vc_ibis_request_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'title' => '',
                    'subtitle' => '',
                    'image2' => '',
                ),
                $atts
            )
        );
        $html = '';
        $html .= '<div class="main-content section main-content-contact">';
        $html .= '<div class="container">';
        $html .=  '<div class="main-content-contact-heading main-heading" id="request">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image, 'full', false )[0] . '" alt="Request a demo">';
        $html .= '<h1>' . $title . '</h1>';
        $html .= '<p>' . $subtitle . '</p>';
        $html .= '</div>';
        $html .= '<div class="form">';
        $html .= '<form method="post">';
        $html .= '<input type="text" name="fullname" placeholder="Name and surname"><br>';
        $html .= '<input type="text" name="company" placeholder="Company"><br>';
        $html .= '<input type="text" name="position" placeholder="Position"><br>';
        $html .= '<input type="text" name="email" placeholder="E-mail"><br>';
        $html .= '<input type="text" name="phone" placeholder="Phone number"><br>';
        $html .= '<label class="textarea-label" for="textarea">Message</label>';
        $html .= '<textarea rows="4" id="textarea" name="message"></textarea>';
        $html .= '<input id="button" type="submit" name="btn_submit2" value="SEND">';
        $html .= '</form>';
        $html .= '<div id="form-message-ibis"></div>';
        require __DIR__ . '/../inc/contact-form2.php';
        $html .= '</div>';
        $html .= '<div class="main-content-about-bottom-image main-content-bottom-image">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image2, 'full', false )[0] . '" alt="Bottom image">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}

new vcIbisRequest();
