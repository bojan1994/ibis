<?php

class vcIbisScrollSpy extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_scroll_spy_mapping' ) );
        add_shortcode( 'vc_ibis_scroll_spy', array( $this, 'vc_ibis_scroll_spy_html' ) );
    }
    public function vc_ibis_scroll_spy_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Scroll Spy Section', 'ibis' ),
                'base' => 'vc_ibis_scroll_spy',
                'description' => __( 'Scroll Spy Section', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-scroll-spy-title',
                        'heading' => __( 'Section title', 'ibis' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Scroll Spy',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-scroll-spy-url',
                        'heading' => __( 'Section url', 'ibis' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Scroll Spy',
                    ),
                )
            )
        );
    }
    public function vc_ibis_scroll_spy_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'url' => '',
                ),
                $atts
            )
        );
        $title_arr = explode( ',', $title );
        $url_arr = explode( ',', $url );
        $section = array_combine( $url_arr, $title_arr );
        $html = '';
        $html .= '<div class="scroll-spy" id="navbar-example">';
            $html .= '<div id="label" class="animated fadeIn">';
                $html .= '<span id="example-id" class="map"></span>';
            $html .= '</div>';
            $html .= '<ul class="span-box nav nav-tabs" role="tablist">';
                foreach( $section as $key => $value ) {
                    $html .= '<li data-title="' . $value . '">';
                        $html .= '<a href="' . $key . '">';
                            $html .= '<span class="class="scroll-spy-span""></span>';
                            $html .= '<span class="class="scroll-spy-span""></span>';
                            $html .= '<span class="class="scroll-spy-span""></span>';
                        $html .= '</a>';
                    $html .= '</li>';
                }
            $html .= '</ul>';
        $html .= '</div>';
        return $html;
    }
}

new vcIbisScrollSpy();
