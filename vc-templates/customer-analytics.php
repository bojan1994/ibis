<?php

class vcIbisCustomerAnalytics extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_customer_analytics_mapping' ) );
        add_shortcode( 'vc_ibis_customer_analytics', array( $this, 'vc_ibis_customer_analytics_html' ) );
    }
    public function vc_ibis_customer_analytics_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Customer Analytics', 'ibis' ),
                'base' => 'vc_ibis_customer_analytics',
                'description' => __( 'Customer Analytics', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-customer-analytics-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Customer Analytics',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'ibis-customer-analytics-title',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Customer Analytics',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-customer-analytics-content',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Customer Analytics',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-customer-analytics-mobile-image1',
                        'heading' => __( 'Image1', 'ibis' ),
                        'param_name' => 'image1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Customer Analytics',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-customer=analytics-bottom-image',
                        'heading' => __( 'Image2', 'ibis' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Customer Analytics',
                    ),
                )
            )
        );
    }
    public function vc_ibis_customer_analytics_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'title' => '',
                    'paragraph' => '',
                    'image1' => '',
                    'image2' => '',
                ),
                $atts
            )
        );
        $html = '';
        $html .= '<div class="main-content section main-content-customer">';
        $html .= '<div class="container">';
        $html .= '<div class="main-content-customer-heading main-heading" id="main-content-customer-heading">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image, 'full', false )[0] . '" alt="Customer Analytics">';
        $html .= '<h2>' . $title . '</h2>';
        $html .= '<p></p>';
        $html .= '</div>';
        $html .= '<div class="main-content-customer-boxes main-content-boxes" id="main-content-customer-boxes">';
        $html .= '<p>' . $paragraph . '</p>';
        $html .= '<div class="main-content-customer-boxes-images main-content-boxes">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image1, 'full', false )[0] . '" alt="Customer Analytics Module">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="main-content-customer-bottom-image main-content-bottom-image">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image2, 'full', false )[0] . '" alt="Bottom image">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}

new vcIbisCustomerAnalytics();
