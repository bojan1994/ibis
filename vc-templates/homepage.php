<?php

class vcIbisHomepage extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_ibis_homepage_mapping' ) );
        add_shortcode( 'vc_ibis_homepage', array( $this, 'vc_ibis_homepage_html' ) );
    }
    public function vc_ibis_homepage_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Homepage', 'ibis' ),
                'base' => 'vc_ibis_homepage',
                'description' => __( 'Homepage', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-first-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-video',
                        'heading' => __( 'Video', 'ibis' ),
                        'param_name' => 'video',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-second-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-homepage-case-study',
                        'heading' => __( 'Case Study', 'ibis' ),
                        'param_name' => 'case_study',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-about-ipi-solution',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-third-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-module-one-image',
                        'heading' => __( 'Module image', 'ibis' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-module-one-title',
                        'heading' => __( 'Module title', 'ibis' ),
                        'param_name' => 'title1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-module-one-content',
                        'heading' => __( 'Module description', 'ibis' ),
                        'param_name' => 'paragraph4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-module-one-label',
                        'heading' => __( 'Label', 'ibis' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-homepage-module-one-url',
                        'heading' => __( 'Module link', 'ibis' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-module-two-image',
                        'heading' => __( 'Module image', 'ibis' ),
                        'param_name' => 'image1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-module-two-title',
                        'heading' => __( 'Module title', 'ibis' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-module-two-content',
                        'heading' => __( 'Module description', 'ibis' ),
                        'param_name' => 'paragraph5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-module-two-label',
                        'heading' => __( 'Label', 'ibis' ),
                        'param_name' => 'label2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-homepage-module-two-url',
                        'heading' => __( 'Module link', 'ibis' ),
                        'param_name' => 'url2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-module-middle-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'ibis-homepage-request-demo-link',
                        'heading' => __( 'Request a demo link label', 'ibis' ),
                        'param_name' => 'demo_link',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-industries-title',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-industries-subtitle',
                        'heading' => __( 'Subtitle', 'ibis' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-industries-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages',
                        'heading' => __( 'Title', 'ibis' ),
                        'param_name' => 'title4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-image1',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-title1',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-image2',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-title2',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-image3',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-title3',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title7',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-ipi-advantages-first-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-second-image1',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image7',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-second-title1',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title8',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-second-image2',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image8',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-second-title2',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title9',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-ipi-advantages-second-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph7',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-third-image1',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image9',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-third-title1',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title10',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-ipi-advantages-third-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph8',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-ipi-advantages-forth-image1',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image10',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-ipi-advantages-forth-title1',
                        'heading' => __( 'Description', 'ibis' ),
                        'param_name' => 'title11',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-ipi-advantages-forth-paragraph',
                        'heading' => __( 'Content', 'ibis' ),
                        'param_name' => 'paragraph9',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-contact-title',
                        'heading' => __( 'Contact title', 'ibis' ),
                        'param_name' => 'title12',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-contact-subtitle',
                        'heading' => __( 'Contact subtitle', 'ibis' ),
                        'param_name' => 'subtitle2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'ibis-homepage-before-footer-image',
                        'heading' => __( 'Image', 'ibis' ),
                        'param_name' => 'image11',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'ibis-homepage-information-title',
                        'heading' => __( 'Information title', 'ibis' ),
                        'param_name' => 'title13',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'ibis-homepage-information',
                        'heading' => __( 'Informations', 'ibis' ),
                        'param_name' => 'paragraph10',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Homepage',
                    ),
                )
            )
        );
    }
    public function vc_ibis_homepage_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'paragraph' => '',
                    'video' => '',
                    'paragraph2' => '',
                    'case_study' => '',
                    'title' => '',
                    'paragraph3' => '',
                    'image' => '',
                    'title1' => '',
                    'paragraph4' => '',
                    'label' => '',
                    'url' => '',
                    'image1' => '',
                    'title2' => '',
                    'paragraph5' => '',
                    'label2' => '',
                    'url2' => '',
                    'image2' => '',
                    'demo_link' => '',
                    'title3' => '',
                    'subtitle' => '',
                    'image3' => '',
                    'title4' => '',
                    'image4' => '',
                    'image5' => '',
                    'image6' => '',
                    'paragraph6' => '',
                    'title5' => '',
                    'title6' => '',
                    'title7' => '',
                    'image7' => '',
                    'image8' => '',
                    'title8' => '',
                    'title9' => '',
                    'paragraph7' => '',
                    'image9' => '',
                    'title10' => '',
                    'paragraph8' => '',
                    'image10' => '',
                    'title11' => '',
                    'paragraph9' => '',
                    'title12' => '',
                    'subtitle2' => '',
                    'image11' => '',
                    'title13' => '',
                    'paragraph10' => '',
                ),
                $atts
            )
        );
        $html = '';
        $html .= '<div class="o-resenju-box section extra-padding" id="o-resenju-box">';
        $html .= '<div class="container">';
        $html .= '<div class="o-resenju-small-box">';
        $html .= '<p>' . $paragraph . '</p>';
        $html .= '</div>';
        $html .= '<video class="video" width="100%" controls> <source src="' . $video . '" type="video/mp4"></video>';
        $html .= '<p>' . $paragraph2 . '</p>';
        $html .= '<a href="#" class="o-resenju-box-button button-animation">' . $case_study . '</a>';
        $html .= '<div class="form o-resenju-form">';
        $html .= '<form id="qwertyuiop" method="post" novalidate>';
        $html .= '<input type="text" name="fullname" placeholder="Name and surname"><br>';
        $html .= '<input type="text" name="company" placeholder="Company"><br>';
        $html .= '<input type="text" name="position" placeholder="Position"><br>';
        $html .= '<input type="email" name="email" placeholder="E-mail"><br>';
        $html .= '<label class="textarea-label" for="textarea">Message</label>';
        $html .= '<textarea rows="4" id="textarea" name="message"></textarea>';
        $html .= '<input id="button" type="submit" name="btn_submit3" value="SEND">';
        $html .= '<p id="error-form-on-submit"></p>';
        $html .= '</form>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="box-shadow-element">';
        $html .= '<img src="http://ibis.test/wp-content/uploads/2018/06/shadow-image.png" alt="Box image">';
        $html .= '</div>';
        $html .= '<div id="about-us" class="prikaz-modula-box section extra-padding">';
        $html .= '<div class="container">';
        $html .= '<h2>' . $title . '</h2>';
        $html .= '<p class="solution-paragraph">' . $paragraph3 . '</p>';
        $html .= '<p class="moduli-paragraph"></p>';
        $html .= '<div class="prikaz-modula-content">';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-4 prikaz-modula-small-box">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image, 'full', false )[0] . '" alt="Network Analytics Module">';
        $html .= '<h4>' . $title1 . '</h4>';
        $html .= '<p>' . $paragraph4 . '</p>';
        $html .= '<a href="' . $url .'" title="Network Analytics Module" class="prikaz-modula-small-box-button">' . $label . '</a>';
        $html .= '</div>';
        $html .= '<div class="col-md-4 prikaz-modula-small-box prikaz-modula-small-box-center">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image2, 'full', false )[0] . '" alt="Ibis Modules">';
        $html .= '</div>';
        $html .= '<div class="col-md-4 prikaz-modula-small-box">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image1, 'full', false )[0] . '" alt="Customer Analytics Module">';
        $html .= '<h4>' . $title2 . '</h4>';
        $html .= '<p>' . $paragraph5 . '</p>';
        $html .= '<a href="' . $url2 . '" title="Customer Analytics Module" class="prikaz-modula-small-box-button">' . $label2 . '</a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prikaz-modula-box-button">';
        $html .= '<a href="request" title="Request a demo link" class="button-animation">' . $demo_link . '</a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="industrije-box section extra-padding" id="industrije-box">';
        $html .= '<div class="container">';
        $html .= '<h2>' . $title3 . '</h2>';
        $html .= '<p class="industrije-paragraph">' . $subtitle . '</p>';
        $html .= '<div class="industrije-small-box-image">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image3, 'full', false )[0] . '" alt="Industries">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednosti-ipi section extra-padding" id="ipi-advantages">';
        $html .= '<div  class="container">';
        $html .= '<h2>' . $title4 . '</h2>';
        $html .= '<p class="prednosti-paragraph"></p>';
        $html .= '<div class="prednost-box">';
        $html .= '<div class="predosti-images">';
        $html .= '<div class="predosti-images-one">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image4, 'full', false )[0] . '" alt="Telco">';
        $html .= '<h4>' . $title5 . '</h4>';
        $html .= '</div>';
        $html .= '<div class="predosti-images-two">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image5, 'full', false )[0] . '" alt="Power">';
        $html .= '<h4>' . $title6 . '</h4>';
        $html .= '</div>';
        $html .= '<div class="predosti-images-three">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image6, 'full', false )[0] . '" alt="Enterpraise">';
        $html .= '<h4>' . $title7 . '</h4>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednosti-text">';
        $html .= '<p>' . $paragraph6 . '</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednost-box">';
        $html .= '<div class="predosti-images">';
        $html .= '<div class="predosti-images-one">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image7, 'full', false )[0] . '" alt="Telco">';
        $html .= '<h4>' . $title8 . '</h4>';
        $html .= '</div>';
        $html .= '<div class="predosti-images-two">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image8, 'full', false )[0] . '" alt="Power">';
        $html .= '<h4>' . $title9 . '</h4>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednosti-text">';
        $html .= '<p>' . $paragraph7 . '</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednost-box">';
        $html .= '<div class="predosti-images">';
        $html .= '<div class="predosti-images-one">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image9, 'full', false )[0] . '" alt="Telco">';
        $html .= '<h4>' . $title10 . '</h4>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednosti-text">';
        $html .= '<p>' . $paragraph8 . '</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednost-box">';
        $html .= '<div class="predosti-images">';
        $html .= '<div class="predosti-images-one">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image10, 'full', false )[0] . '" alt="Power">';
        $html .= '<h4>' . $title11 . '</h4>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="prednosti-text">';
        $html .= '<p>' . $paragraph9 . '</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="contact-box section extra-padding" id="contact-us">';
        $html .= '<div class="container">';
        $html .= '<h2>' . $title12 . '</h2>';
        $html .= '<p class="contact-paragraph">' . $subtitle2 . '</p>';
        $html .= '<div class="form">';
        $html .= '<form method="post">';
        $html .= '<input type="text" name="fullname" placeholder="Name and surname"><br>';
        $html .= '<input type="text" name="email" placeholder="E-mail"><br>';
        $html .= '<label class="textarea-label" for="textarea">Message</label>';
        $html .= '<textarea rows="4" id="textarea" name="message"></textarea>';
        $html .= '<input id="button" type="submit" name="btn_submit1" value="SEND">';
        $html .= '</form>';
        $html .= '<div id="form-message-ibis"></div>';
        require __DIR__ . '/../inc/contact-form1.php';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="main-content-contact-boxes main-content-boxes">';
        $html .= '<div class="container">';
        $html .= '<div class="main-content-contact-small-boxes main-content-contact-small-boxes-first main-content-small-boxes">';
        $html .= '<img src="' . wp_get_attachment_image_src( $image11, 'full', false )[0] . '" alt="Ibis Instruments">';
        $html .= '</div>';
        $html .= '<div class="main-content-contact-small-boxes main-content-small-boxes">';
        $html .= '<div class="main-content-contact-small-boxes-top">';
        $html .= '<div class="contact-column">';
        $html .= '<h5>' . $title13 . '</h5>';
        $html .= '<p>' . $paragraph10 . '</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}

new vcIbisHomepage();
