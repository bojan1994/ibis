<?php

class vcIbisTest extends WPBakeryShortCode {
    function __construct() {
        add_action( 'vc_before_init', array( $this, 'vc_ibis_test_mapping' ) );
        add_shortcode( 'vc_ibis_test', array( $this, 'vc_ibis_test_html' ) );
    }
    public function vc_ibis_test_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Test', 'ibis' ),
                'base' => 'vc_ibis_test',
                'description' => __( 'Test', 'ibis' ),
                'category' => __( 'Ibis Elements', 'ibis' ),
                'params' => array(
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'div',
                        'class' => 'ibis-test',
                        'heading' => __( 'Test', 'ibis' ),
                        'param_name' => 'content',
                        "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "ibis" ),
                        "description" => __( "Enter your content.", "ibis" )
                    ),
                )
            )
        );
    }
    // public function vc_ibis_test_html( $atts ) {
    //     extract(
    //         shortcode_atts(
    //             array(
    //                 'content' => '',
    //             ),
    //             $atts
    //         )
    //     );
    //     //$html = $content;
    //     // $html .= '<p>'.$content.'</p>';
    //     return $content;
    // }
}

new vcIbisTest();
