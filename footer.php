        <div class="box-shadow-element">
            <img src="<?php bloginfo('template_url'); ?>/images/shadow-image.png" alt="Box image">
        </div>

        <footer class="section" id="footer">
            <div class="container">
                <div class="footer-image">
                    <?php
                    if( is_active_sidebar( 'footer1' )) {
                        dynamic_sidebar( 'footer1' );
                    }
                    ?>
                </div>
                <div class="row footer-row">
                    <?php
                    $count_posts = wp_count_posts();
                    if( $count_posts->publish > 0 ) {
                        $args = array(
                            'theme_location' => 'footer',
                        );
                        wp_nav_menu( $args );
                    }
                    else {
                        $args = array(
                            'theme_location' => 'footer2',
                        );
                        wp_nav_menu( $args );
                    }
                    ?>
                </div>
                <div class="footer-social">
                    <a href="<?php echo get_theme_mod( 'custom_facebook_url' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/facebook.png" alt="Facebook"></a>
                    <a href="<?php echo get_theme_mod( 'custom_twitter_url' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png" alt="Twitter"></a>
                    <a href="<?php echo get_theme_mod( 'custom_instagram_url' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/instagram.png" alt="Instagram"></a>
                    <a href="<?php echo get_theme_mod( 'custom_linkedin_url' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/linkedin.png" alt="Linkedin"></a>
                </div>
                <div class="footer-second-paragraph">
                    <p><?php echo get_theme_mod( 'custom_footer_text' ); ?></p>
                </div>
            </div>
        </footer>

        <?php wp_footer(); ?>

    </body>

</html>
