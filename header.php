<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <header class="navbar navbar-default navigation-bar noOpacity navbar-fixed-top">
            <nav class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                        <?php
                        $custom_logo = get_theme_mod( 'custom_logo', array( 'class' => 'img-responsive' ) );
                        $image = wp_get_attachment_image_src( $custom_logo, 'full' );
                        ?>
                        <img src="<?php echo $image[0]; ?>" alt="<?php bloginfo( 'name' ); ?>">
                    </a>
                    <p class="logo-paragraph"><?php bloginfo( 'description' ); ?></p>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="primary-navigation">
                    <?php
                    $count_posts = wp_count_posts();
                    if( $count_posts->publish > 0 ) {
                        $args = array(
                            'theme_location' => 'primary',
                            'menu_class' => 'nav navigation-list noOpacity navbar-nav navbar-right',
                        );
                        wp_nav_menu( $args );
                    }
                    else {
                        $args = array(
                            'theme_location' => 'secondary',
                            'menu_class' => 'nav navigation-list noOpacity navbar-nav navbar-right',
                        );
                        wp_nav_menu( $args );
                    }
                    ?>
                </div>
            </nav>
        </header>
