<?php

$args = array(
	'post_type' => 'slider',
);
$query = new WP_Query( $args );
if( $query->have_posts() ) : ?>
    <div id="main-carousel" class="carousel section slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <?php
            $counter = 0;
                while( $query->have_posts() ) :
                    $counter++;
                    $query->the_post(); ?>
                        <div class="item<?php echo ($counter == 1) ? ' active' : ''; ?>">
                            <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => 'Homepage slider' ) ); ?>
                            <div class="carousel-caption">
                                <h1><?php the_title(); ?></h1>
                                <?php
                                the_content();
                                $contact_us_label = get_post_meta( $post->ID, '_contact_value_key', true );
                                $contact_us_url = get_post_meta( $post->ID, '_contact_url_value_key', true );
                                ?>
                                <a href="<?php echo $contact_us_url; ?>" class="carousel-button carousel-button-animation"><?php echo $contact_us_label; ?></a>
                            </div>
                        </div>
            <?php endwhile;
            wp_reset_query(); ?>
            <ol class="carousel-indicators">
            <?php
            for($i=0; $i<$counter; $i++) {
                ?> <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
            <?php } ?>
             </ol>
        </div>
    </div>
<?php else :
    get_template_part( '../content', 'none' );
endif;
