<?php

get_header();

if( have_posts() ) : ?>
    <div class="container single-blog-container">
    <?php while( have_posts() ) :
        the_post(); ?>
        <div class="single-blog-content">
            <?php the_post_thumbnail( 'blog-image-full', array( 'class' => 'img-responsive', 'alt' => 'Post image' ) ); ?>
            <div class="single-blog-text">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile; ?>
        <div class="blog-navigation single-blog-navigation"><p><?php previous_post_link( '%link', '« Previous', TRUE ); ?>    <?php next_post_link( '%link', 'Next »', TRUE ); ?></p></div>
    </div>
    <?php
endif;

get_footer();
